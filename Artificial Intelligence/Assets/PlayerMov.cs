﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class PlayerMov : NetworkBehaviour
{

    public float turnsmoothing = 15f;
    public float speedDamping = .1f;
    public GameObject others;
    public GameObject bulletObject;
    public Transform bulletPostition;
    private Animator anim;
    private AudioSource audio;

    Transform sword;
    Transform hand;
   
    bool holding = false;
    bool dodging = false;
    
    public GameObject camera;
  
    void Start()
    {       
        if (isLocalPlayer)
        {
            camera.SetActive(true);
        }
        
    }
    void Awake()
    {
        //others.SetActive(false);

        audio = GetComponent<AudioSource>();
        hand = GameObject.FindGameObjectWithTag("Hand").transform;
      //  sword = GameObject.FindGameObjectWithTag("SwordHandle").transform;
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Move();

        if (Input.GetMouseButtonDown(0))
        {
            CmdShoot();
        }

        if (Input.GetKeyDown("1"))
        {
            anim.SetLayerWeight(1, 1f);
            anim.SetInteger("CurrentAction", 1);
        }
        if(Input.GetKeyUp("1"))
        {
            anim.SetInteger("CurrentAction", 0);   
        }
        if (Input.GetKey("2"))
        {
            if (anim.GetBool("Dodge") == false)
            {
                anim.SetBool("Dodge", true);
                Invoke("cancelStuff", .2f);
            }
            
        }
        if (Input.GetKey("3"))
        {
            if (anim.GetBool("Attack") == false)
            {
                anim.SetBool("Attack", true);
                Invoke("cancelStuff", .2f);
            }
        }
        if (Input.GetKey("4"))
            {
                if (anim.GetBool("Salute") == false)
                {
                    anim.SetBool("Salute", true);
                    Invoke("cancelStuff", .2f);
                }
            }
        if(Input.GetKey("6"))
        {
            if(anim.GetBool("Dance") == false)
            {
                
                Invoke("Dance", 3.5f);
                //Invoke("cancelStuff", 2f);
                audio.Play();
            }
        }


    }
    void Dance()
    {
        anim.SetBool("Dance", true);
        others.SetActive(true);
       // Invoke("Dance", .1f);
    }
    void cancelStuff()
    {
        anim.SetBool("Dodge", false);
        anim.SetBool("Attack", false);
        anim.SetBool("Salute", false);
        anim.SetBool("Dance", false);
    }
    void Move()
    {
        anim.SetFloat("Speed", Input.GetAxis("Vertical"));
        anim.SetFloat("Turning", Input.GetAxis("Horizontal"));
        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetFloat("Speed", 5f, speedDamping, Time.deltaTime);
        }
       

    }
    void StopDodging()
    {
        anim.SetLayerWeight(1, 0);
    }

    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Sword" && holding == false)
        {            
            if (Input.GetKey(KeyCode.E))
            {
                sword.parent = hand.parent;
                
                sword.position = hand.position;

                sword.rotation = hand.rotation * Quaternion.Euler(275, 0, 0);
            }
          
        }
    }
    [Command]
    void CmdRespawn()
    {
        var spawn = NetworkManager.singleton.GetStartPosition();
        var newPlayer = (GameObject)Instantiate(NetworkManager.singleton.playerPrefab, spawn.position, spawn.rotation);
        NetworkServer.Destroy(this.gameObject);
        NetworkServer.ReplacePlayerForConnection(this.connectionToClient, newPlayer, this.playerControllerId);
    }
    [Command]
    void CmdShoot()
    {
        var bullet = (GameObject)Instantiate(bulletObject, bulletPostition.position, bulletPostition.rotation);

        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 20;
        
        NetworkServer.Spawn(bullet);

        Destroy(bullet, 10);
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "BallOfDeath" || col.gameObject.tag == "Bullet")
        {
            CmdRespawn();
        }

    }
    void drop()
    {
      
    }

}
 

    

     




