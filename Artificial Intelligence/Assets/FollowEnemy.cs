﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FollowEnemy : NetworkBehaviour
{

    Transform target;
    NavMeshAgent agent;
    List<GameObject> playerList;// = new List<GameObject>();
    private GameObject [] player;

    NetworkView netView;
    
	// Use this for initialization
	void Start () {
        playerList = new List<GameObject>();
        agent = GetComponent<NavMeshAgent>();
        netView = GetComponent<NetworkView>();

        player = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in player)
        {
            playerList.Add(p);
        }
    }
	
	// Update is called once per frame
	void Update () {
             
        if(playerList.Count == 2)
        {
            findEnemy();
        }

	
	}
    void OnPlayerConnected()
    {
        playerList.Clear();
        player = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in player)
        {
            playerList.Add(p);
        }
    }


    
    void OnCollisionEnter(Collision col)
    {
        if(netView.isMine && (col.gameObject.tag == "Bullet" || col.gameObject.tag == "Player" || col.gameObject.tag=="BallOfDeath"))
        {
            Network.Destroy(this.gameObject);
        }
    }
    void findEnemy()
    {
        Transform enemyPosition = playerList[1].gameObject.transform;
        agent.SetDestination(enemyPosition.position);

    }
}
