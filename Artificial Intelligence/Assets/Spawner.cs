﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Spawner : NetworkBehaviour
{ 
    public GameObject ballOfDeath;
    Vector3 spawn1= new Vector3(-88,3,88);
    Vector3 spawn2 = new Vector3(-12, 3, 10);

	// Use this for initialization
	void Start () {
        CmdSpawnMore();
    }
	
	// Update is called once per frame
	void Update () {

        int noOfBalls = CheckNumberOfBalls();
        if(noOfBalls < 1)
        {
            CmdSpawnMore();
        }
	}

    int CheckNumberOfBalls()
    {
          var balls = GameObject.FindGameObjectsWithTag("BallOfDeath");
          return balls.Length;
    }
    [Command]
    void CmdSpawnMore()
    {
        GameObject enemy1 = (GameObject)Instantiate(ballOfDeath, spawn1, Quaternion.identity);
        NetworkServer.Spawn(enemy1);
        GameObject enemy2 = (GameObject)Instantiate(ballOfDeath, spawn2, Quaternion.identity);
        NetworkServer.Spawn(enemy2);
    }
}
