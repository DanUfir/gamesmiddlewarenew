﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
public class PatrollingScript : MonoBehaviour {

    List<Transform> patrolPoints = new List<Transform>();
    GameObject patrolContainer;
    private int destinationPoint = 0;
    private NavMeshAgent meshAgent;

    
    float detectionDistance = 30f; 
    
    void Awake()
    {
        patrolContainer = GameObject.FindGameObjectWithTag("Patrols");
        Transform[] patrolObjects = patrolContainer.GetComponentsInChildren<Transform>();

        foreach(Transform child in patrolObjects)
        {
            patrolPoints.Add(child);
        }
    }
    // Use this for initialization
    void Start () {
        meshAgent = GetComponent<NavMeshAgent>();
        meshAgent.autoBraking = false;
        destinationPoint = Random.RandomRange(0, 8);
        meshAgent.SetDestination(patrolPoints[destinationPoint].transform.position);        
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(destinationPoint);
        // Patrol();
        Debug.DrawRay(transform.position, transform.forward * 30, Color.green);

        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, transform.forward, out hit, 30))
        {
            if (hit.transform.gameObject.tag == "Player")
            {
                Attack(hit);
                Debug.Log("Hi");   
                Debug.Log(meshAgent.destination);
            }
        }
        else Patrol();       
    }
    void Attack(RaycastHit hit)
    {
        meshAgent.destination = hit.transform.position;
        meshAgent.speed *= 1.5f;
    }

    void Patrol()
    {
        if(patrolPoints.Count == 0)
        {
            return;
        }
        float ss = Vector3.Distance(this.transform.position, patrolPoints[destinationPoint].transform.position);
       // Debug.Log(ss);
        
        if (Vector3.Distance(this.transform.position, patrolPoints[destinationPoint].transform.position) <= 2f)
        {
            
            destinationPoint++;
            if (destinationPoint >= patrolPoints.Count)
            {
                destinationPoint = 0;
            }            
            meshAgent.destination = patrolPoints[destinationPoint].position;
          
        }
    }
    
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player" || col.gameObject.tag == "Bullet")
        {
            NetworkServer.Destroy(this.gameObject);
        }
    }
}
