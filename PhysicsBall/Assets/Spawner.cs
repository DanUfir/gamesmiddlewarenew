﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public GameObject ball;
    private float xCorRange = 19;
    private float zCorRange = 14;
    private int counter = 0;
    public AudioSource sound;

	// Use this for initialization
	void Start () {
          StartCoroutine("spawnObjects");
        
    }
	
	// Update is called once per frame
	void Update () {
       

    }
    IEnumerator spawnObjects()
    {
        while (counter < 30)
        {
            Instantiate(ball, new Vector3(Random.RandomRange(-xCorRange, xCorRange), Random.RandomRange(20, 50), Random.RandomRange(-zCorRange, zCorRange)), Quaternion.identity);
            counter++;
            sound.Play();
            
            yield return new WaitForSeconds(Random.RandomRange(5, 10));
        }
        yield return new WaitForSeconds(Random.RandomRange(5, 10));
    }
}
