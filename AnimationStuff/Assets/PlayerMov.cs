﻿using UnityEngine;
using System.Collections;

public class PlayerMov : MonoBehaviour
{

    public float turnsmoothing = 15f;
    public float speedDamping = .1f;

    private Animator anim;


    Transform sword;
    Transform hand;
   
    bool holding = false;
    bool dodging = false;

    void Awake()
    {
        hand = GameObject.FindGameObjectWithTag("Hand").transform;
        sword = GameObject.FindGameObjectWithTag("SwordHandle").transform;
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        Move();
        

        if (Input.GetKeyDown("1"))
        {
            anim.SetLayerWeight(1, 1f);
            anim.SetInteger("CurrentAction", 1);
        }
        if(Input.GetKeyUp("1"))
        {
            anim.SetInteger("CurrentAction", 0);   
        }
        if (Input.GetKey("2"))
        {
            if (anim.GetBool("Dodge") == false)
            {
                anim.SetBool("Dodge", true);
                Invoke("cancelStuff", .2f);
            }
            
        }
        if (Input.GetKey("3"))
        {
            if (anim.GetBool("Attack") == false)
            {
                anim.SetBool("Attack", true);
                Invoke("cancelStuff", .2f);
            }
        }
        if (Input.GetKey("4"))
            {
                if (anim.GetBool("Salute") == false)
                {
                    anim.SetBool("Salute", true);
                    Invoke("cancelStuff", .2f);
                }
            }


    }
    void cancelStuff()
    {
        anim.SetBool("Dodge", false);
        anim.SetBool("Attack", false);
        anim.SetBool("Salute", false);
    }
    void Move()
    {
        anim.SetFloat("Speed", Input.GetAxis("Vertical"));
        anim.SetFloat("Turning", Input.GetAxis("Horizontal"));
        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetFloat("Speed", 5f, speedDamping, Time.deltaTime);
        }
       

    }
    void StopDodging()
    {
        anim.SetLayerWeight(1, 0);
    }

    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Sword" && holding == false)
        {            
            if (Input.GetKey(KeyCode.E))
            {
                sword.parent = hand.parent;
                
                sword.position = hand.position;

                sword.rotation = hand.rotation * Quaternion.Euler(275, 0, 0);
            }
          
        }
    }
    void drop()
    {
      
    }

}
 

    

     




